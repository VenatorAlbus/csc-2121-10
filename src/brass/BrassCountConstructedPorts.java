package brass;

import util.Execute;
import util.CountCommand;

import brass.*;

public class BrassCountConstructedPorts implements CountCommand<BrassIndustry> {
	private int count;

	public BrassCountConstructedPorts() {
		count = 0;
	}

	public void execute(BrassIndustry item) {
		if(item.isConstructedPort())
			count++;
	}

	public int getCount() {
		return count;
	}
}