package brass;

import util.Execute;
import util.CountCommand;

import brass.*;

public class BrassCountPlayersUnflippedIndustry implements CountCommand<BrassIndustry> {
	private int count;
	private int industry;
	private int player;

	public BrassCountPlayersUnflippedIndustry(int ind, int pl) {
		count = 0;
		industry = ind;
		player = pl;
	}

	public void execute(BrassIndustry item) {
		if(item.isUnflippedIndustry(industry) && item.getPlayerID() == player)
			count++;
	}

	public int getCount() {
		return count;
	}
}