package brass;

import util.Execute;
import util.CountCommand;

import brass.*;

public class BrassCountUnflippedIndustry implements CountCommand<BrassIndustry> {
	private int count;
	private int industry;

	public BrassCountUnflippedIndustry(int ind) {
		count = 0;
		industry = ind;
	}

	public void execute(BrassIndustry item) {
		if(item.isUnflippedIndustry(industry))
			count++;
	}

	public int getCount() {
		return count;
	}
}